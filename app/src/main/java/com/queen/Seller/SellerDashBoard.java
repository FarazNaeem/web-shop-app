package com.queen.Seller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.queen.Seller.Models.PieSales;
import com.queen.gapshop.R;

import java.util.ArrayList;

public class SellerDashBoard extends AppCompatActivity implements OnChartValueSelectedListener {

    private LineChart lineChart;
    private PieChart pieChart;
    private PieChart pieChart2;
    private ArrayList<PieEntry> pieEntries;
    private ArrayList<PieSales> productSolde;
    private LinearLayout pieLinear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_dash_board);
        lineChart = findViewById(R.id.line_chart);
        pieChart2 = findViewById(R.id.pie_chart2);
        pieLinear=findViewById(R.id.pie_linear);
        pieChart2.setClickable(true);
        pieChart2.setOnChartValueSelectedListener(this);
        pieChart2.setTouchEnabled(true);
        ArrayList<Entry> values = new ArrayList<>();
        pieEntries = new ArrayList<>();
        productSolde = new ArrayList<>();
        testPieEntries();
        for(int i=0;i<productSolde.size();i++){
            pieEntries.add(new PieEntry(productSolde.get(i).getNumberOfProductSales()));
        }
        PieDataSet pieDataSet=new PieDataSet(pieEntries,"Most Product Sold");
        pieDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        pieDataSet.setValueTextSize(15);
        Description description= new Description();
        description.setText("");
        description.setTextColor(getResources().getColor(R.color.bt_white_87));
        pieChart2.setDescription(description);

        pieChart2.setCenterText("Products");
        pieChart2.setCenterTextColor(getResources().getColor(R.color.chartBg));
        PieData pieData= new PieData(pieDataSet);
        pieChart2.setData(pieData);
        Legend legend=pieChart2.getLegend();
        legend.setDrawInside(false);
        legend.setTextColor(getResources().getColor(R.color.bt_white_87));
//        legend.setWordWrapEnabled(true);
        pieChart2.animateXY(2000,2000);
        pieChart2.invalidate();

        pieChart = findViewById(R.id.pie_chart);
        pieChart.setClickable(true);
        pieChart.setOnChartValueSelectedListener(this);
        pieChart.setTouchEnabled(true);

        pieChart.setDescription(description);

        pieChart.setCenterText("Products top");
        pieChart.setCenterTextColor(getResources().getColor(R.color.chartBg));
        pieChart.setData(pieData);
        Legend legend2=pieChart.getLegend();
        legend2.setDrawInside(false);
        legend2.setTextColor(getResources().getColor(R.color.bt_white_87));
//        legend.setWordWrapEnabled(true);
        pieChart.animateXY(2000,2000);
        pieChart.invalidate();
        values.add(new Entry(1, 12));
        values.add(new Entry(2, 18));
        values.add(new Entry(3, 29));
        values.add(new Entry(4, 25));
        values.add(new Entry(5, 22));
        values.add(new Entry(6, 29));
        values.add(new Entry(7, 40));
        values.add(new Entry(8, 32));
        values.add(new Entry(9, 36));
        values.add(new Entry(10, 40));

        LineDataSet set1;

        if (lineChart.getData() != null && lineChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) lineChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            lineChart.getData().notifyDataChanged();
            lineChart.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "Products Sale");
            set1.setDrawIcons(false);
            set1.enableDashedLine(20f, 5f, 1f);
            set1.enableDashedHighlightLine(20f, 5f, 1f);
            set1.setColor(Color.DKGRAY);
            set1.setCircleColor(Color.DKGRAY);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 2f));
            set1.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.graph_background);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.DKGRAY);
            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            LineData data = new LineData(dataSets);
            lineChart.setData(data);
        }
        LineDataSet set2;
        if (lineChart.getData() != null &&
                lineChart.getData().getDataSetCount() > 0) {
            set2 = (LineDataSet) lineChart.getData().getDataSetByIndex(0);
            set2.setValues(values);
            lineChart.getData().notifyDataChanged();
            lineChart.notifyDataSetChanged();
        } else {
            set2 = new LineDataSet(values, "Sample Data");
            set2.setDrawIcons(false);
            set2.enableDashedLine(20f, 5f, 1f);
            set2.enableDashedHighlightLine(20f, 5f, 1f);
            set2.setColor(Color.DKGRAY);
            set2.setCircleColor(Color.DKGRAY);
            set2.setLineWidth(1f);
            set2.setCircleRadius(3f);
            set2.setDrawCircleHole(false);
            set2.setValueTextSize(9f);
            set2.setDrawFilled(true);
            set2.setFormLineWidth(1f);
            set2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 2f));
            set2.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.graph_background);
                set2.setFillDrawable(drawable);
            } else {
                set2.setFillColor(Color.DKGRAY);
            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set2);
            LineData data = new LineData(dataSets);
            lineChart.setData(data);
        }

    }

    private void testPieEntries() {
        productSolde.add(new PieSales("Clothing", 200));
        productSolde.add(new PieSales("under Wear", 25));
        productSolde.add(new PieSales("Makeup", 80));
        productSolde.add(new PieSales("Mobiles", 50));
    }

    @Nullable
    @Override
    public View onCreateView(@Nullable View parent, @NonNull String name, @NonNull Context context, @NonNull AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        startActivity(new Intent(SellerDashBoard.this,SalesDetail.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.add_product){

        }
        return super.onOptionsItemSelected(item);
    }
}