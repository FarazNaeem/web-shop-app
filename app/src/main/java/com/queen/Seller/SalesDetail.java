package com.queen.Seller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.queen.Seller.Models.PieSales;
import com.queen.gapshop.R;

import java.util.ArrayList;

public class SalesDetail extends AppCompatActivity {

    private PieChart pieChart;
    private ArrayList<PieEntry> pieEntries;
    private ArrayList<PieSales> productSolde;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_detail);
        pieChart = findViewById(R.id.pie_chart);
        ArrayList<Entry> values = new ArrayList<>();
        pieEntries = new ArrayList<>();
        productSolde = new ArrayList<>();
        testPieEntries();
        for(int i=0;i<productSolde.size();i++){
            pieEntries.add(new PieEntry(productSolde.get(i).getNumberOfProductSales(),productSolde.get(i).getProductName()));
        }
        PieDataSet pieDataSet=new PieDataSet(pieEntries,"Most Product Sold");
        pieDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        pieDataSet.setValueTextSize(15);
        Description description= new Description();
        description.setText("Detail Graph");
        description.setTextColor(getResources().getColor(R.color.bt_white_87));
        pieChart.setDescription(description);
        pieChart.setCenterText("Products");
        pieChart.setCenterTextColor(getResources().getColor(R.color.chartBg));
        PieData pieData= new PieData(pieDataSet);
        pieChart.setData(pieData);
        Legend legend=pieChart.getLegend();
        legend.setDrawInside(false);
        legend.setTextColor(getResources().getColor(R.color.bt_white_87));
        legend.setWordWrapEnabled(true);
        pieChart.animateXY(2000,2000);
        pieChart.invalidate();
    }
    private void testPieEntries() {
        productSolde.add(new PieSales("Clothing", 200));
        productSolde.add(new PieSales("under Wear", 25));
        productSolde.add(new PieSales("Makeup", 80));
        productSolde.add(new PieSales("Mobiles", 50));
    }
}