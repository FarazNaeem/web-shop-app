package com.queen.Seller.Models;

public class PieSales {
    private String productName;
    private int numberOfProductSales;

    public PieSales(String productName, int numberOfProductSales) {
        this.productName = productName;
        this.numberOfProductSales = numberOfProductSales;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getNumberOfProductSales() {
        return numberOfProductSales;
    }

    public void setNumberOfProductSales(int numberOfProductSales) {
        this.numberOfProductSales = numberOfProductSales;
    }
}
