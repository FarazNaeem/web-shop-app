package com.queen.gapshop.ViewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.Utils.FirebaseUtils;

import java.util.ArrayList;

public class FavouriteViewModel extends ViewModel {
    private MutableLiveData<ArrayList<ProductModel>> myFavs= new MutableLiveData<>();
    private static FavouriteViewModel instance;
    public static FavouriteViewModel getInstance(){
        if (instance == null)
            instance= new FavouriteViewModel();
        return instance;
    }
    public MutableLiveData<ArrayList<ProductModel>> loadMyFavs(){
        myFavs= FirebaseUtils.getInstance().getMyFav();
        return myFavs;
    }
    public MutableLiveData<Boolean> getIsGettingFavs(){
        return FirebaseUtils.getInstance().getIsGettingFav();
    }
    public void addFavToCart(ProductModel model){
        FirebaseUtils.getInstance().addProductToMyCart(model);
    }
}
