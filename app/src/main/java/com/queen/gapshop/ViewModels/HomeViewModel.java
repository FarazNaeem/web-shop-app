package com.queen.gapshop.ViewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.FirebaseFirestore;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.Utils.FirebaseUtils;

import java.util.ArrayList;

public class HomeViewModel extends ViewModel {
    private static HomeViewModel instace;
    private MutableLiveData<ArrayList<ProductModel>> womenProducts;
    private MutableLiveData<ArrayList<ProductModel>> sportsProducts;
    private MutableLiveData<ArrayList<ProductModel>> recentProducts;
    public static HomeViewModel getInstance() {
        if (instace == null)
            instace = new HomeViewModel();
        return instace;
    }
    public MutableLiveData<ArrayList<ProductModel>> getRecentProducts(){
        recentProducts= FirebaseUtils.getInstance().getRecent();
        return recentProducts;
    }
    public MutableLiveData<Boolean> getIsGettingRecents(){
        return FirebaseUtils.getInstance().getIsGettingRecent();
    }
    public void addFav(ProductModel model) {
        FirebaseUtils.getInstance().addFav(model);
    }

    public MutableLiveData<ArrayList<ProductModel>> getWomenProducts() {
        womenProducts = FirebaseUtils.getInstance().getWomenProducts();
        return womenProducts;
    }

    public MutableLiveData<Boolean> getIsGettingWomenProducts() {
        return FirebaseUtils.getInstance().getIsGettingWomenProduct();
    }

    public MutableLiveData<ArrayList<ProductModel>> getSportsProducts() {
        sportsProducts = FirebaseUtils.getInstance().getSportsProducts();
        return sportsProducts;
    }
    public MutableLiveData<Boolean> getIsGettingSportsProduct(){
        return FirebaseUtils.getInstance().getIsGettingSportsProduct();
    }
}
