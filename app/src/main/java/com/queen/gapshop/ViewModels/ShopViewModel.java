package com.queen.gapshop.ViewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.Utils.FirebaseUtils;

import java.util.ArrayList;

public class ShopViewModel extends ViewModel {
    private MutableLiveData<ArrayList<ProductModel>> myProducts= new MutableLiveData<>();
    private static ShopViewModel instance;
    private double price=0;
    public void CalculateTotal(){
        if(myProducts==null){
            return ;
        }
        for(int i=0;i<myProducts.getValue().size();i++){
            price=price+myProducts.getValue().get(i).getProductPrice();
        }

    }
    public double getPrice(){
        return price;
    }
    public static ShopViewModel getInstance(){
        if (instance == null)
            instance= new ShopViewModel();
        return instance;
    }
    public MutableLiveData<Boolean> getIsGettingCart(){
        return FirebaseUtils.getInstance().getIsGettingCart();
    }
    public MutableLiveData<ArrayList<ProductModel>> loadMyProducts(){
        myProducts= FirebaseUtils.getInstance().getMyCart();
        return myProducts;
    }
    public MutableLiveData<ArrayList<ProductModel>> getMyProducts(){
        return myProducts;
    }
}
