package com.queen.gapshop.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.queen.gapshop.R;

public class UniversalImageLoader {
    private static final int defaultImage= R.drawable.ic_home;
    private final Context context;


    public UniversalImageLoader(Context context){
        this.context=context;
    }
    public ImageLoaderConfiguration getConfig(){
        DisplayImageOptions defaultoptions=new DisplayImageOptions.Builder()
                .showImageOnLoading(defaultImage)
                .showImageForEmptyUri(defaultImage)
                .showImageOnFail(defaultImage)
                .cacheOnDisk(true)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .resetViewBeforeLoading(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(1000))
                .build();
        ImageLoaderConfiguration configuration=new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultoptions)
                .memoryCache(new WeakMemoryCache())
                .threadPriority(Thread.MAX_PRIORITY)
                .threadPoolSize(5)

                .diskCacheSize(100*1024*1024)
                .build();
        return configuration;
    }
    public static void setImage(String imageUri, ImageView imageView, String append){
        ImageLoader imageLoader=ImageLoader.getInstance();
        imageLoader.displayImage(append + imageUri, imageView, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                //progressBar.setVisibility(View.GONE);
                Log.e("universalImageLoader", "onLoadingFailed: image loading failed "+imageUri );
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                //progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                //progressBar.setVisibility(View.GONE);
            }
        });
    }
}
