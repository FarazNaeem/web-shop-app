package com.queen.gapshop.Utils;

import android.os.storage.OnObbStateChangeListener;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crashlytics.internal.common.CrashlyticsCore;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.ProductDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FirebaseUtils {
    private static String TAG="Utils";
    private static FirebaseUtils instance;
    private MutableLiveData<ArrayList<ProductModel>> products = new MutableLiveData<>();
    private MutableLiveData<Boolean> isGettingProducts = new MutableLiveData<>();
    private ArrayList<ProductModel> productList = new ArrayList<>();
    private MutableLiveData<ArrayList<ProductModel>> BannerProducts = new MutableLiveData<>();
    private MutableLiveData<Boolean> isGettingBannerProducts = new MutableLiveData<>();
    private ArrayList<ProductModel> bannerProductList = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ArrayList<ProductModel> cartList= new ArrayList<>();
    private MutableLiveData<ArrayList<ProductModel>> cartProducts= new MutableLiveData<>();
    private MutableLiveData<Boolean> isGettingCart= new MutableLiveData<>();
    private MutableLiveData<ArrayList<ProductModel>> myFav= new MutableLiveData<>();
    private ArrayList<ProductModel> favList= new ArrayList<>();
    private MutableLiveData<Boolean> isGettingFav= new MutableLiveData<>();
    private MutableLiveData<ArrayList<ProductModel>> womenProducts= new MutableLiveData<>();
    private ArrayList<ProductModel> wProductList= new ArrayList<>();
    private MutableLiveData<Boolean> isGettingWomenProduct= new MutableLiveData<>();
    private MutableLiveData<ArrayList<ProductModel>> sportsProducts= new MutableLiveData<>();
    private ArrayList<ProductModel> sProductList= new ArrayList<>();
    private MutableLiveData<Boolean> isGettingSportsProduct= new MutableLiveData<>();
    private MutableLiveData<ArrayList<ProductModel>> recentProducts= new MutableLiveData<>();
    private ArrayList<ProductModel> recentList= new ArrayList<>();
    private MutableLiveData<Boolean> isGettingRecent= new MutableLiveData<>();
    public static FirebaseUtils getInstance() {
        if (instance == null)
            instance = new FirebaseUtils();
        return instance;
    }
    public MutableLiveData<ArrayList<ProductModel>> getBannerProducts() {
        isGettingBannerProducts.setValue(true);
        final Query query;
        db.collection("productsBanner")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ProductModel model;
                        for (DocumentSnapshot d : queryDocumentSnapshots.getDocuments()) {
                            model = d.toObject(ProductModel.class);
                            System.out.println("sex "+d);
                            bannerProductList.add(model);
                        }
                        BannerProducts.setValue(productList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("sex", "onFailure: ", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                BannerProducts.setValue(productList);
                isGettingBannerProducts.postValue(false);
            }
        });
        BannerProducts.setValue(bannerProductList);
        return this.BannerProducts;
    }
    public MutableLiveData<ArrayList<ProductModel>> getProducts() {
        isGettingProducts.setValue(true);
        final Query query;
        db.collection("products")
                .whereEqualTo("productFor","men")
        .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ProductModel model;
                        for (DocumentSnapshot d : queryDocumentSnapshots.getDocuments()) {
                            model = d.toObject(ProductModel.class);
                            System.out.println("sex "+d);
                            productList.add(model);
                         }
                        products.setValue(productList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("sex", "onFailure: ", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                products.setValue(productList);
                isGettingProducts.postValue(false);
            }
        });
        products.setValue(productList);
        return this.products;
    }
    public void test(){
        db.collection("products")
                .document()
                .set(new ProductModel());
    }
    public MutableLiveData<Boolean> getIsGettingProducts() {
        return isGettingProducts;
    }
    public MutableLiveData<Boolean> getIsGettingBannerProducts() {
        return isGettingBannerProducts;
    }
    public void addProductToMyCart(ProductModel model){
        db.collection("users")
                .document("faraz")
                .collection("mycart")
                .document()
                .set(model)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sex", "onFailure: add my cart", e);
                    }
                });
    }
    public MutableLiveData<ArrayList<ProductModel>> getMyCart(){
        isGettingCart.setValue(true);
        db.collection("users")
                .document("faraz")
                .collection("mycart")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ProductModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(ProductModel.class);
                            cartList.add(model);
                        }
                        cartProducts.setValue(cartList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sex", "onFailure: on getting cart", e);
                    }
                }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                cartProducts.setValue(cartList);
                isGettingCart.postValue(false);
            }
        });
        cartProducts.setValue(cartList);
        return  cartProducts;
    }
    public MutableLiveData<Boolean> getIsGettingCart(){
        return isGettingCart;
    }
    public void addFav(ProductModel model){
        Map<String, Object> map= new HashMap<>();
        db.collection("users")
                .document("faraz")
                .collection("fav")
                .document()
                .set(model)
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("sex", "onFailure: on add fav",e );
            }
        });
    }
    public MutableLiveData<ArrayList<ProductModel>> getMyFav(){
        isGettingFav.setValue(true);
        db.collection("users")
                .document("faraz")
                .collection("fav")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ProductModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(ProductModel.class);
                            favList.add(model);
                        }
                        myFav.setValue(favList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                isGettingFav.postValue(false);
            }
        });
        myFav.setValue(favList);
        return myFav;
    }
    public MutableLiveData<Boolean> getIsGettingFav(){
        return  isGettingFav;
    }
    public MutableLiveData<ArrayList<ProductModel>> getWomenProducts(){
        isGettingWomenProduct.setValue(true);
        db.collection("products")
                .whereEqualTo("productFor","women")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ProductModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(ProductModel.class);
                            wProductList.add(model);
                        }
                        womenProducts.setValue(wProductList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure: get product for women", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                isGettingWomenProduct.postValue(false);
            }
        });
        womenProducts.setValue(wProductList);
        return womenProducts;
    }
    public MutableLiveData<Boolean> getIsGettingWomenProduct(){
        return isGettingWomenProduct;
    }
    public MutableLiveData<ArrayList<ProductModel>> getSportsProducts(){
        isGettingSportsProduct.setValue(true);
        db.collection("products")
                .whereEqualTo("productCategory","sportswear")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ProductModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(ProductModel.class);
                            sProductList.add(model);
                        }
                        sportsProducts.setValue(sProductList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure: get product for women", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                isGettingSportsProduct.postValue(false);
            }
        });
        sportsProducts.setValue(sProductList);
        return sportsProducts;
    }
    public MutableLiveData<Boolean> getIsGettingSportsProduct(){
        return isGettingSportsProduct;
    }
    public MutableLiveData<ArrayList<ProductModel>> getRecent(){
        isGettingRecent.setValue(true);
        db.collection("users")
                .document("faraz")
                .collection("recent")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            recentList.add(d.toObject(ProductModel.class));
                        }
                        recentProducts.setValue(recentList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure: on getting recent", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                recentProducts.postValue(recentList);
                isGettingRecent.postValue(false);

            }
        });
        recentProducts.setValue(recentList);
        return recentProducts;
    }
    public MutableLiveData<Boolean> getIsGettingRecent(){
        return isGettingRecent;
    }
}
