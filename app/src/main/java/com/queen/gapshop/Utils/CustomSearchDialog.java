package com.queen.gapshop.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.queen.gapshop.Adaptors.RecentAdaptor;
import com.queen.gapshop.Adaptors.SearchResultAdaptor;
import com.queen.gapshop.Interfaces.SearchClickCallBacks;
import com.queen.gapshop.Interfaces.onRecentLoads;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.R;
import com.queen.gapshop.ViewModels.HomeViewModel;

import java.util.ArrayList;

public class CustomSearchDialog extends Dialog implements onRecentLoads {
    public Activity activity;
    private RecyclerView recentRecycler,resultRecycler;
    private LinearLayout recentLayout,resultLayout;
    private ProgressBar progressBar;
    private EditText searchParams;
    private static String TAG="Dialog";
    private ArrayList<ProductModel> searchListResults;
    private SearchResultAdaptor resultAdaptor;
    private RecentAdaptor recentAdaptor;
    private LifecycleOwner owner;

    public CustomSearchDialog(@NonNull Activity activity) {
        super(activity,android.R.style.ThemeOverlay);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        this.activity=activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_search_dialog);
        __INIT__();
        initTextListener();
        updateSearchRecent(HomeViewModel.getInstance().getRecentProducts().getValue());
//        HomeViewModel.getInstance().getIsGettingRecents().observe();

    }
    private void initTextListener() {
        Log.d(TAG, "initTextListener: ider aya hai kerword lay ker");
        searchListResults = new ArrayList<>();
        searchParams.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d(TAG, "beforeTextChanged: beforesfadkladsf");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d(TAG, "onTextChanged: chagneadslkfj");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String searchQuery = searchParams.getText().toString().trim();
                searchForMatchKeyword(searchQuery);
            }
        });
    }

    private void searchForMatchKeyword(String keyword) {
        Log.d(TAG, "searchForMatchKeyword: ider aya hai search kerne");
        Toast.makeText(activity, "baba g ider b aya ", Toast.LENGTH_SHORT).show();
        searchListResults.clear();
        if (keyword.length() == 0) {
            searchListResults.clear();
        } else {
            FirebaseFirestore.getInstance()
                    .collection("products")
                    .orderBy("productName")
                    .startAt(keyword)
                    .endAt(keyword+"\uf8ff")
                    .limit(3).get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            ProductModel model;
                            for (DocumentSnapshot d : queryDocumentSnapshots.getDocuments()) {
                                model = d.toObject(ProductModel.class);
                                searchListResults.add(model);
                                updateSearchResults(searchListResults);
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure: on searching in box", e);
                }
            });


        }
    }
    private void updateSearchResults(ArrayList<ProductModel> searchListResults){
        recentLayout.setVisibility(View.GONE);
        resultLayout.setVisibility(View.VISIBLE);
        resultAdaptor= new SearchResultAdaptor(activity,searchListResults);
        resultRecycler.setLayoutManager(new LinearLayoutManager(activity));
        resultRecycler.setAdapter(resultAdaptor);
    }
    private void updateSearchRecent(ArrayList<ProductModel> recentProducts){
        recentAdaptor= new RecentAdaptor(activity,recentProducts);
        recentRecycler.setLayoutManager(new LinearLayoutManager(activity));
        recentRecycler.setAdapter(resultAdaptor);
    }
    private void __INIT__() {
        searchParams=findViewById(R.id.search_box);
        progressBar=findViewById(R.id.progress);
        recentLayout=findViewById(R.id.recent_layout);
        resultLayout=findViewById(R.id.result_layout);
        recentRecycler=findViewById(R.id.recent_recycler);
        resultRecycler=findViewById(R.id.result_recycler);
    }

    @Override
    public void onRecentLoaded() {
        recentAdaptor.notifyDataSetChanged();
    }
}
