package com.queen.gapshop.Utils;

import android.util.Log;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavHelper {
    private static final String TAG="BottomNavHelper";
    public static void SetupBottomNavigationView(BottomNavigationView bottomNavigationView,final int id){
        Log.d(TAG, "SetupBottomNavigationView: ");

        bottomNavigationView.setItemHorizontalTranslationEnabled(false);
        bottomNavigationView.setHovered(false);
    }
}
