package com.queen.gapshop.Utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.queen.gapshop.R;

public class FavMoreDialog extends Dialog {

    public FavMoreDialog(@NonNull Context context) {
        super(context,android.R.style.ThemeOverlay);
        getWindow().setLayout(600,300);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_more);

    }
}
