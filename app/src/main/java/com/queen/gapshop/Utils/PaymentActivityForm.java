package com.queen.gapshop.Utils;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.queen.gapshop.R;

public class PaymentActivityForm extends AppCompatActivity {
    private CardForm cardForm;
    private AlertDialog.Builder alertBuilder;
    private Button donateMoneyNow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_form);

        cardForm=findViewById(R.id.card_form2);

        donateMoneyNow=findViewById(R.id.donatee);
        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .actionLabel("Donate")
                .setup(PaymentActivityForm.this)
        ;
        cardForm.getCvvEditText().setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        donateMoneyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cardForm.isValid()){
                    alertBuilder=new AlertDialog.Builder(PaymentActivityForm.this);
                    alertBuilder.setTitle("Confirm Before Donate");
                    alertBuilder.setMessage("You are going to Donate 1 USD");
                    alertBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();;
                            Toast.makeText(PaymentActivityForm.this, "thanks for donating!!!", Toast.LENGTH_LONG).show();
                        }
                    });
                    alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();;
                        }
                    });
                    alertBuilder.create();
                    alertBuilder.show();
                }else{

                }

            }
        });
    }
}