package com.queen.gapshop.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.R;
import com.queen.gapshop.SearchResults;

import java.util.ArrayList;

public class SearchResultAdaptor extends RecyclerView.Adapter<SearchResultAdaptor.ItemViewHolder>{

    private Context context;
    private ArrayList<ProductModel> products;
    public SearchResultAdaptor(Context context,ArrayList<ProductModel> products){
        this.context=context;
        this.products=products;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_items,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.productName.setText(products.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TextView productName;
        private LinearLayout resultLayot;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            productName=itemView.findViewById(R.id.product_name);
            resultLayot=itemView.findViewById(R.id.layout_result);
            resultLayot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent= new Intent(context, SearchResults.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("query",products.get(getAdapterPosition()).getProductName());
                    context.startActivity(intent);
                }
            });
        }
    }
}
