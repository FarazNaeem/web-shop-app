package com.queen.gapshop.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.ProductDetail;
import com.queen.gapshop.R;

import java.util.ArrayList;

public class WomenAdaptor extends RecyclerView.Adapter<WomenAdaptor.ItemViewHolder> {

    private Context context;
    private ArrayList<ProductModel> products;
    private ImageLoader imageLoader;
    public WomenAdaptor(Context context,ArrayList<ProductModel> products){
        this.context=context;
        this.products=products;
        imageLoader=ImageLoader.getInstance();
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.women_item,parent,false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        imageLoader.displayImage(products.get(position).getProductThumbnailUrl(),holder.productImage);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView productImage;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            productImage=itemView.findViewById(R.id.women_product_image);
            productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("imageUrl",products.get(getAdapterPosition()).getProductThumbnailUrl());
                    intent.putExtra("name","NO name");
                    intent.putExtra("flag","a");
                    context.startActivity(intent);
                }
            });
        }
    }
}
