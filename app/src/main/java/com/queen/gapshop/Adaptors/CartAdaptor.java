package com.queen.gapshop.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.ProductDetail;
import com.queen.gapshop.R;
import com.queen.gapshop.ShopFeed;

import java.util.ArrayList;

public class CartAdaptor extends RecyclerView.Adapter<CartAdaptor.ItemViewHolder> {
    public Context context1;
    private ArrayList<ProductModel> products;
    private ImageLoader imageLoader;

    public CartAdaptor(Context context, ArrayList<ProductModel> products) {
        this.context1 = context;
        this.products = products;
        imageLoader = ImageLoader.getInstance();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_items, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        imageLoader.displayImage(products.get(position).getProductThumbnailUrl(), holder.imageView);
        holder.sellerName.setText(products.get(position).getProductSellerName());
        holder.price.setText(products.get(position).getProductPrice() + "Pkr");
        holder.sizes.setText("XL");

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView,deleteIcon;
        private TextView sellerName, sizes, price;
        private RelativeLayout relativeLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            deleteIcon=itemView.findViewById(R.id.ic_delete);
            relativeLayout = itemView.findViewById(R.id.layout_relative);
            imageView = itemView.findViewById(R.id.cart_image);
            sellerName = itemView.findViewById(R.id.seller_name);
            sizes = itemView.findViewById(R.id.cart_product_size);
            price = itemView.findViewById(R.id.cart_product_price);

            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context1, "delete click", Toast.LENGTH_SHORT).show();

                }
            });
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context1, ProductDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("flag", "c");
                    intent.putExtra("imageUrl", products.get(getAdapterPosition()).getProductThumbnailUrl());
                    intent.putExtra("name", products.get(getAdapterPosition()).getProductName());
                    context1.startActivity(intent);
                }
            });
        }
    }
}
