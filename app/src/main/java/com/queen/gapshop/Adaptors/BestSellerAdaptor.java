package com.queen.gapshop.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.FavouriteFeed;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.ProductDetail;
import com.queen.gapshop.R;
import com.queen.gapshop.ViewModels.HomeViewModel;

import java.util.ArrayList;

public class BestSellerAdaptor extends RecyclerView.Adapter<BestSellerAdaptor.ItemViewHolder> {
    private ArrayList<ProductModel> products;
    private ImageLoader imageLoader;
    private Context context;
    public BestSellerAdaptor(Context context, ArrayList<ProductModel> products) {
        this.products = products;
        this.context=context;
        imageLoader= ImageLoader.getInstance();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.best_seller_items,parent,false);

        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        imageLoader.displayImage(products.get(position).getProductThumbnailUrl(),holder.brandImage);
        holder.brandPrice.setText(products.get(position).getProductPrice()+"");
        holder.brandName.setText(products.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView brandImage,favIcon;
        private TextView brandName, brandPrice;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            favIcon=itemView.findViewById(R.id.fav_add);
            brandImage = itemView.findViewById(R.id.best_image1);
            brandName = itemView.findViewById(R.id.best_brand_name);
            brandPrice = itemView.findViewById(R.id.best_price);
            favIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HomeViewModel.getInstance().addFav(products.get(getAdapterPosition()));
                    Intent intent= new Intent(context, FavouriteFeed.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(intent);
                }
            });
            brandImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("imageUrl",products.get(getAdapterPosition()).getProductThumbnailUrl());
                    intent.putExtra("name",products.get(getAdapterPosition()).getProductName());
                    intent.putExtra("flag","a");
                    context.startActivity(intent);
                }
            });
        }
    }
}
