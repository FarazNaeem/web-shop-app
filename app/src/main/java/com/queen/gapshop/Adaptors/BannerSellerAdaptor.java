package com.queen.gapshop.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.ProductDetail;
import com.queen.gapshop.R;

import java.util.ArrayList;

public class BannerSellerAdaptor extends RecyclerView.Adapter<BannerSellerAdaptor.ItemViewHolder> {
    private ArrayList<ProductModel> products;
    private ImageLoader imageLoader;
    private Context context;
    public BannerSellerAdaptor(Context context ,ArrayList<ProductModel> products) {
        this.products = products;
        this.context=context;
        imageLoader= ImageLoader.getInstance();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_banner_view_home,parent,false);

        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        imageLoader.displayImage(products.get(position).getProductThumbnailUrl(),holder.brandImage);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView brandImage;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            brandImage = itemView.findViewById(R.id.banner_image);
            brandImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent= new Intent(context, ProductDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("imageUrl",products.get(getAdapterPosition()).getProductThumbnailUrl());
                    intent.putExtra("flag","b");
                    context.startActivity(intent);
                }
            });
        }
    }
}
