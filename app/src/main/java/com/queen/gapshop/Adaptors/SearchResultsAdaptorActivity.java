package com.queen.gapshop.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.ProductDetail;
import com.queen.gapshop.R;
import com.queen.gapshop.SearchResults;

import java.util.ArrayList;

public class SearchResultsAdaptorActivity extends RecyclerView.Adapter<SearchResultsAdaptorActivity.ItemViewHolder>{

    private Context context;
    private ArrayList<ProductModel> products;
    private ImageLoader imageLoader;
    public SearchResultsAdaptorActivity(Context context,ArrayList<ProductModel> products){
        this.context=context;
        this.products=products;
        imageLoader= ImageLoader.getInstance();
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_search_results,parent,false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if(products.get(position)!= null){
            imageLoader.displayImage(products.get(position).getProductThumbnailUrl(),holder.productImage);
            holder.productPrice.setText(products.get(position).getProductPrice()+"");
            holder.productName.setText(products.get(position).getProductName());
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TextView productName,productPrice,product2Name,product2Price;
        private ImageView productImage,product2Image;
        private LinearLayout linearLayout1,linearLayout2;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            productName=itemView.findViewById(R.id.product_name);
            product2Name=itemView.findViewById(R.id.product2_name);
            productPrice=itemView.findViewById(R.id.product_price);
            product2Price=itemView.findViewById(R.id.product2_price);
            productImage=itemView.findViewById(R.id.product_image);
            product2Image=itemView.findViewById(R.id.product2_image);
            linearLayout1=itemView.findViewById(R.id.layout_search1);
            linearLayout2=itemView.findViewById(R.id.layout_search2);
            linearLayout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent= new Intent(context, ProductDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("flag","e");
                    intent.putExtra("imageUrl",products.get(getAdapterPosition()).getProductThumbnailUrl());
                    intent.putExtra("name",products.get(getAdapterPosition()).getProductName());
                    intent.putExtra("query",products.get(getAdapterPosition()).getProductName());
                    context.startActivity(intent);
                }
            });
        }
    }
}
