package com.queen.gapshop.Adaptors;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.TimedText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.ProductDetail;
import com.queen.gapshop.R;
import com.queen.gapshop.ShopFeed;
import com.queen.gapshop.Utils.FavMoreDialog;
import com.queen.gapshop.ViewModels.FavouriteViewModel;

import java.util.ArrayList;

public class FavouriteAllAdaptor extends RecyclerView.Adapter<FavouriteAllAdaptor.ItemViewHolder>{

    private Context context;
    private ArrayList<ProductModel> myFavs;
    private ImageLoader imageLoader;
    public FavouriteAllAdaptor(Context context,ArrayList<ProductModel> myFavs){
        this.context=context;
        this.myFavs=myFavs;
        imageLoader=ImageLoader.getInstance();
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_items,parent,false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        imageLoader.displayImage(myFavs.get(position).getProductThumbnailUrl(),holder.cartImage);
        holder.productName.setText(myFavs.get(position).getProductName());
        holder.price.setText(myFavs.get(position).getProductPrice()+"PKR");
        holder.desc.setText(myFavs.get(position).getProductDescription());
    }

    @Override
    public int getItemCount() {
        return myFavs.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        ImageView cartImage,more;
        TextView productName,desc,price;
        RelativeLayout relativeLayout;
        Button addCart;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            addCart=itemView.findViewById(R.id.to_cart);
            price=itemView.findViewById(R.id.price);
            cartImage=itemView.findViewById(R.id.cart_image);
            more=itemView.findViewById(R.id.more);
            productName=itemView.findViewById(R.id.cart_product_name);
            desc=itemView.findViewById(R.id.short_desc);
            relativeLayout=itemView.findViewById(R.id.layout_relative);

            addCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProductModel model= new ProductModel();
                    model.setProductName(myFavs.get(getAdapterPosition()).getProductName());
                    model.setProductThumbnailUrl(myFavs.get(getAdapterPosition()).getProductThumbnailUrl());
                    model.setProductDescription(myFavs.get(getAdapterPosition()).getProductDescription());
                    model.setProductPrice(myFavs.get(getAdapterPosition()).getProductPrice());
                    FavouriteViewModel.getInstance().addFavToCart(model);
                    context.startActivity(new Intent(context, ShopFeed.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .putExtra("imageUrl",myFavs.get(getAdapterPosition()).getProductThumbnailUrl())
                            .putExtra("name",myFavs.get(getAdapterPosition()).getProductName()));
                }
            });
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent= new Intent(context, ProductDetail.class);
                    intent.putExtra("imageUrl",myFavs.get(getAdapterPosition()).getProductThumbnailUrl());
                    intent.putExtra("name",myFavs.get(getAdapterPosition()).getProductName());
                    intent.putExtra("flag","c");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(intent);
                }
            });
            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.item_more);
                    dialog.show();
                }
            });
        }
    }
}
