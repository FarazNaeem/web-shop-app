package com.queen.gapshop.Adaptors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class ViewPagerAdaptorHome extends FragmentPagerAdapter {
    private final ArrayList<Fragment> mFragmentlist= new ArrayList<>();
    private final ArrayList<String> mTitlelist= new ArrayList<>();
    public ViewPagerAdaptorHome(@NonNull FragmentManager fm) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentlist.get(position);
    }

    @Override
    public int getCount() {
        return mTitlelist.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitlelist.get(position);
    }

    public void addFragments(Fragment fragment,String title){
        mFragmentlist.add(fragment);
        mTitlelist.add(title);
    }
}
