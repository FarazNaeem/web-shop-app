package com.queen.gapshop.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.R;

import java.util.ArrayList;

public class RecentAdaptor  extends RecyclerView.Adapter<RecentAdaptor.ItemViewHolder>{
    private Context context;
    private ArrayList<ProductModel> products;
    public RecentAdaptor(Context context,ArrayList<ProductModel> products){
        this.context=context;
        this.products=products;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_items,parent,false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if(products.get(position) != null){
            holder.productName.setText(products.get(position).getProductName());
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private TextView productName;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            productName=itemView.findViewById(R.id.product_name);
        }
    }
}
