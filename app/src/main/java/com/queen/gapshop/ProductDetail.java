package com.queen.gapshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.Utils.BottomSheetDialogFragment;
import com.queen.gapshop.Utils.FirebaseUtils;

public class ProductDetail extends AppCompatActivity {

    private ImageView more;
    private ImageLoader imageLoader;
    private String imageUrl,prodname;
    private ImageView prodImage;
    private TextView prodName;
    private Button addToCart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        __INIT__();
        if(getIntent().getStringExtra("flag").equals("b")){
            imageUrl=getIntent().getStringExtra("imageUrl");
            setUpDetailsforBannerProduct();

        }else{
            imageUrl=getIntent().getStringExtra("imageUrl");
            prodname=getIntent().getStringExtra("name");
            setUPDetails();
        }
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialogFragment bottomSheetDialogFragment=new BottomSheetDialogFragment();
                bottomSheetDialogFragment.show(getSupportFragmentManager(),"homeBottom");
            }
        });
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductModel model= new ProductModel();
                model.setProductThumbnailUrl(imageUrl);
                model.setProductName(prodname);
                FirebaseUtils.getInstance().addProductToMyCart(model);
                startActivity(new Intent(ProductDetail.this,ShopFeed.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                .putExtra("imageUrl",imageUrl)
                                .putExtra("name",prodname));
            }
        });
    }

    private void setUpDetailsforBannerProduct() {
        imageLoader.displayImage(imageUrl,prodImage);
    }

    private void setUPDetails() {
        imageLoader.displayImage(imageUrl,prodImage);
        prodName.setText(prodname);
    }

    private void __INIT__() {

        addToCart=findViewById(R.id.add_to_cart);
        prodImage=findViewById(R.id.detail_image);
        prodName=findViewById(R.id.detail_brand_name);
        more=findViewById(R.id.more_info);
        imageLoader= ImageLoader.getInstance();
    }
}