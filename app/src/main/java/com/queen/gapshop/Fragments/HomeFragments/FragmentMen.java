package com.queen.gapshop.Fragments.HomeFragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.queen.gapshop.Adaptors.BannerSellerAdaptor;
import com.queen.gapshop.Adaptors.BestSellerAdaptor;
import com.queen.gapshop.R;
import com.queen.gapshop.Utils.FirebaseUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentMen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMen extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProgressBar progressBar;
    private BestSellerAdaptor adaptor;
    private BannerSellerAdaptor bannerSellerAdaptor;
    private RecyclerView recyclerView;
    private RecyclerView bannerRecycler;
    public FragmentMen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMen.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentMen newInstance(String param1, String param2) {
        FragmentMen fragment = new FragmentMen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    private void __INIT__(View v) {

    }
    private void setUpRecycler(Context context){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_men, container, false);
        progressBar=v.findViewById(R.id.progress);
        recyclerView=v.findViewById(R.id.best_seller);
        bannerRecycler=v.findViewById(R.id.banner_recycler);
        adaptor= new BestSellerAdaptor(getActivity(),FirebaseUtils.getInstance().getProducts().getValue());
        bannerSellerAdaptor= new BannerSellerAdaptor(getActivity(),FirebaseUtils.getInstance().getBannerProducts().getValue());
        recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext(),LinearLayoutManager.HORIZONTAL,false));
        bannerRecycler.setLayoutManager(new LinearLayoutManager(container.getContext()));
        recyclerView.setAdapter(adaptor);
        bannerRecycler.setAdapter(bannerSellerAdaptor);
        FirebaseUtils.getInstance().getIsGettingProducts().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    progressBar.setVisibility(View.GONE);
                    adaptor.notifyDataSetChanged();
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        FirebaseUtils.getInstance().getIsGettingBannerProducts().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    progressBar.setVisibility(View.GONE);
                    bannerSellerAdaptor.notifyDataSetChanged();
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        return  v;
    }
}