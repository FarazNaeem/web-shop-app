package com.queen.gapshop.Fragments.HomeFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.queen.gapshop.Adaptors.WomenAdaptor;
import com.queen.gapshop.R;
import com.queen.gapshop.ViewModels.HomeViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentWomen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentWomen extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private WomenAdaptor adaptor;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentWomen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentWomen.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentWomen newInstance(String param1, String param2) {
        FragmentWomen fragment = new FragmentWomen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_women, container, false);
        recyclerView=v.findViewById(R.id.women_recycler);
        progressBar=v.findViewById(R.id.progress);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adaptor=new WomenAdaptor(getActivity(), HomeViewModel.getInstance().getWomenProducts().getValue());
        recyclerView.setAdapter(adaptor);
        HomeViewModel.getInstance().getIsGettingWomenProducts().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    progressBar.setVisibility(View.GONE);
                    adaptor.notifyDataSetChanged();

                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        return v;
    }
}