package com.queen.gapshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.queen.gapshop.Adaptors.SearchResultsAdaptorActivity;
import com.queen.gapshop.Models.ProductModel;

import java.util.ArrayList;

public class SearchResults extends AppCompatActivity {

    private ArrayList<ProductModel> searchListResults= new ArrayList<>();
    private static String TAG="SearchResults";
    private RecyclerView recyclerView;
    private SearchResultsAdaptorActivity adaptorActivity;
    private MutableLiveData<Boolean> isFetching= new MutableLiveData<>();
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        String query=getIntent().getStringExtra("query");
        __INIT__();
        if(query != null){
            searchForMatchKeyword(query);
            isFetching.observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    if(!aBoolean){
                        adaptorActivity.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }
            });
        }


    }

    private void __INIT__() {
        progressBar=findViewById(R.id.progress);
        recyclerView=findViewById(R.id.search_result_recycler);
    }

    private void searchForMatchKeyword(String keyword) {
        isFetching.setValue(true);
            FirebaseFirestore.getInstance()
                    .collection("products")
                    .orderBy("productName")
                    .startAt(keyword)
                    .endAt(keyword+"\uf8ff")
                    .limit(3).get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            ProductModel model;
                            for (DocumentSnapshot d : queryDocumentSnapshots.getDocuments()) {
                                model = d.toObject(ProductModel.class);
                                searchListResults.add(model);

                            }
                            updateSearchResults(searchListResults);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure: on searching in box", e);
                }
            }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    isFetching.postValue(false);
                }
            });
    }

    private void updateSearchResults(ArrayList<ProductModel> searchListResults) {
        adaptorActivity=new SearchResultsAdaptorActivity(this,searchListResults);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptorActivity);
    }
}