package com.queen.gapshop.Models;

import java.util.ArrayList;

public class ProductModel {


    private String sellerId;
    private String productName,productDescription,productAdditionalInfo;
    private ArrayList<String> productAvailableSizes;
    private String productCategory;
    private ArrayList<String> productImages;
    private double productPrice;
    private String productThumbnailUrl;
    private String productSellerName,productFor;
    private String productBrandName;

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductBrandName() {
        return productBrandName;
    }
    public void setProductBrandName(String productBrandName) {
        this.productBrandName = productBrandName;
    }
    public String getProductFor() {
        return productFor;
    }

    public void setProductFor(String productFor) {
        this.productFor = productFor;
    }

    public String getProductSellerName() {
        return productSellerName;
    }

    public void setProductSellerName(String productSellerName) {
        this.productSellerName = productSellerName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductAdditionalInfo() {
        return productAdditionalInfo;
    }

    public void setProductAdditionalInfo(String productAdditionalInfo) {
        this.productAdditionalInfo = productAdditionalInfo;
    }

    public ArrayList<String> getProductAvailableSizes() {
        return productAvailableSizes;
    }

    public void setProductAvailableSizes(ArrayList<String> productAvailableSizes) {
        this.productAvailableSizes = productAvailableSizes;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public ArrayList<String> getProductImages() {
        return productImages;
    }

    public void setProductImages(ArrayList<String> productImages) {
        this.productImages = productImages;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductThumbnailUrl() {
        return productThumbnailUrl;
    }

    public void setProductThumbnailUrl(String productThumbnailUrl) {
        this.productThumbnailUrl = productThumbnailUrl;
    }
}
