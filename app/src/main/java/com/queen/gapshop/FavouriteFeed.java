package com.queen.gapshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.queen.gapshop.Adaptors.ViewPagerAdaptorFavourite;
import com.queen.gapshop.Fragments.FragmentsFav.FragmentAll;
import com.queen.gapshop.Fragments.FragmentsFav.FragmentCollections;
import com.queen.gapshop.Utils.BottomNavHelper;

public class FavouriteFeed extends AppCompatActivity {

    private Toolbar toolbar;
    private ViewPagerAdaptorFavourite mViewPagerAdaptor;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private BottomNavigationView mBottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_feed);
        __INIT__();
        setSupportActionBar(toolbar);
        mViewPagerAdaptor= new ViewPagerAdaptorFavourite(getSupportFragmentManager());
        mViewPagerAdaptor.addFragments(new FragmentAll(),"All");
        mViewPagerAdaptor.addFragments(new FragmentCollections(),"My Collections");
        mViewPager.setAdapter(mViewPagerAdaptor);
        mTabLayout.setupWithViewPager(mViewPager);
        BottomNavHelper.SetupBottomNavigationView(mBottomNavigationView,R.id.nav_fav);
        BottomNavigationViewListener();
    }

    private void __INIT__() {

        toolbar=findViewById(R.id.toolbar);
        mViewPager=findViewById(R.id.view_pager);
        mTabLayout=findViewById(R.id.layout_tab);
        mBottomNavigationView=findViewById(R.id.bottomnav);
    }
    /**
     * A listener for bottom navigation
     * start the activity which is click
     * do not start new activity rather bring it front if exists
     */
    private void BottomNavigationViewListener(){
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_home:
                        startActivity(new Intent(FavouriteFeed.this,HomeFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_search:
                        startActivity(new Intent(FavouriteFeed.this,SearchFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_fav:
                        return true;
                    case R.id.nav_shop:
                        startActivity(new Intent(FavouriteFeed.this,ShopFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_account:
                        startActivity(new Intent(FavouriteFeed.this,AccountFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;

                }
                return false;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favourite_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}