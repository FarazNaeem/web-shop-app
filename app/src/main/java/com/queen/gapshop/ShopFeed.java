package com.queen.gapshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.queen.gapshop.Adaptors.CartAdaptor;
import com.queen.gapshop.Address.AddressActivity;
import com.queen.gapshop.Models.ProductModel;
import com.queen.gapshop.Utils.BottomNavHelper;
import com.queen.gapshop.Utils.FirebaseUtils;
import com.queen.gapshop.ViewModels.ShopViewModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Objects;

public class ShopFeed extends AppCompatActivity {

    private BottomNavigationView mBottomNavigationView;
    private CartAdaptor cartAdaptor;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView totalPrice;
    private Toolbar toolbar;
    private Button orderNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_feed);
        __INIT__();
//        progressBar.getIndeterminateDrawable().setColorFilter(0x80ffb600, PorterDuff.Mode.MULTIPLY);
        setSupportActionBar(toolbar);
        BottomNavigationViewListener();
        BottomNavHelper.SetupBottomNavigationView(mBottomNavigationView,R.id.nav_shop);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        cartAdaptor= new CartAdaptor(this, ShopViewModel.getInstance().loadMyProducts().getValue());
        recyclerView.setAdapter(cartAdaptor);
        ShopViewModel.getInstance().getIsGettingCart().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    cartAdaptor.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                    ShopViewModel.getInstance().CalculateTotal();
                    setPrice(ShopViewModel.getInstance().getPrice());
                    if(ShopViewModel.getInstance().getMyProducts().getValue().size() > 0){
                        getSupportActionBar().setTitle("My Basket - "+ShopViewModel.getInstance().getMyProducts().getValue().size()+" Products");
                    }

                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        orderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(ShopFeed.this, AddressActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
    }
    private void setPrice(double price){

        Double truncate= BigDecimal.valueOf(price)
                .setScale(3, RoundingMode.FLOOR)
                .doubleValue();
        totalPrice.setText("Total "+truncate+" PKR");
    }
    private void __INIT__() {
        orderNow=findViewById(R.id.order_now);
        toolbar=findViewById(R.id.toolbar);
        totalPrice=findViewById(R.id.total_price);
        progressBar=findViewById(R.id.progress);
        recyclerView=findViewById(R.id.shop_recycler);
        mBottomNavigationView=findViewById(R.id.bottomnav);
    }
    /**
     * A listener for bottom navigation
     * start the activity which is click
     * do not start new activity rather bring it front if exists
     */
    private void BottomNavigationViewListener(){


        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_home:

                        startActivity(new Intent(ShopFeed.this,HomeFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));

                        return true;
                    case R.id.nav_search:
                        startActivity(new Intent(ShopFeed.this,SearchFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_fav:
                        startActivity(new Intent(ShopFeed.this,FavouriteFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_shop:
                        return true;
                    case R.id.nav_account:
                        startActivity(new Intent(ShopFeed.this,AccountFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;

                }
                return false;
            }
        });
    }

}