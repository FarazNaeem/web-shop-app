package com.queen.gapshop.Address;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.queen.gapshop.R;
import com.queen.gapshop.Utils.PaymentActivityForm;

public class AddressActivity extends AppCompatActivity {

    private Button confirm ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        confirm=findViewById(R.id.conferm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(AddressActivity.this, PaymentActivityForm.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent,3369);
            }
        });
    }
}