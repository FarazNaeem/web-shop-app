package com.queen.gapshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.firestore.model.Values;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.gapshop.Adaptors.ViewPagerAdaptorHome;
import com.queen.gapshop.Fragments.HomeFragments.FragmentElectronics;
import com.queen.gapshop.Fragments.HomeFragments.FragmentMen;
import com.queen.gapshop.Fragments.HomeFragments.FragmentSupperMarket;
import com.queen.gapshop.Fragments.HomeFragments.FragmentSupports;
import com.queen.gapshop.Fragments.HomeFragments.FragmentWomen;
import com.queen.gapshop.Utils.BottomNavHelper;
import com.queen.gapshop.Utils.CustomSearchDialog;
import com.queen.gapshop.Utils.UniversalImageLoader;

public class HomeFeed extends AppCompatActivity {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private ViewPagerAdaptorHome mViewPagerAdaptorHome;
    private BottomNavigationView mBottomNavigationView;
    private EditText searchLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_feed);
        __INIT__();
        mViewPagerAdaptorHome = new ViewPagerAdaptorHome(getSupportFragmentManager());
        mViewPagerAdaptorHome.addFragments(new FragmentMen(),"Men");
        mViewPagerAdaptorHome.addFragments(new FragmentWomen(),"Women");
        mViewPagerAdaptorHome.addFragments(new FragmentSupports(),"Supports");
        mViewPagerAdaptorHome.addFragments(new FragmentElectronics(),"Electronics");
        mViewPagerAdaptorHome.addFragments(new FragmentSupperMarket(),"Supper Market");
        mViewPager.setAdapter(mViewPagerAdaptorHome);
        mTabLayout.setupWithViewPager(mViewPager);
        BottomNavHelper.SetupBottomNavigationView(mBottomNavigationView,R.id.nav_home);
        BottomNavigationViewListener();
        initImageLoader();
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomSearchDialog dialog= new CustomSearchDialog(HomeFeed.this);
                dialog.show();
            }
        });
    }

    /**
     * A listener for bottom navigation
     * start the activity which is click
     * do not start new activity rather bring it front if exists
     */
    private void BottomNavigationViewListener(){
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_home:

                        return true;
                    case R.id.nav_search:
                        startActivity(new Intent(HomeFeed.this,SearchFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_fav:
                        startActivity(new Intent(HomeFeed.this,FavouriteFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_shop:
                        startActivity(new Intent(HomeFeed.this,ShopFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_account:
                        startActivity(new Intent(HomeFeed.this,AccountFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;

                }
                return false;
            }
        });
    }

    /**
     * initialize widgets
     */
    private void __INIT__() {

        searchLayout=findViewById(R.id.search_home);
        mBottomNavigationView=findViewById(R.id.bottomnav);
        mViewPager=findViewById(R.id.pager_home);
        mTabLayout=findViewById(R.id.layout_tab);
    }
    private void initImageLoader() {
        UniversalImageLoader universalImageLoader=new UniversalImageLoader(getApplicationContext());
        ImageLoader.getInstance().init(universalImageLoader.getConfig());

    }
}