package com.queen.gapshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.queen.gapshop.Adaptors.ViewPagerAdaptorHome;
import com.queen.gapshop.Fragments.AccountFragments.AboutDeveloper;
import com.queen.gapshop.Fragments.AccountFragments.CoupponFragment;
import com.queen.gapshop.Fragments.AccountFragments.Help;
import com.queen.gapshop.Fragments.AccountFragments.MyMessages;
import com.queen.gapshop.Fragments.AccountFragments.MyOrders;
import com.queen.gapshop.Fragments.AccountFragments.MyReview;
import com.queen.gapshop.Fragments.AccountFragments.PinStored;
import com.queen.gapshop.Fragments.AccountFragments.Settings;
import com.queen.gapshop.Utils.BottomNavHelper;

public class AccountFeed extends AppCompatActivity {

    private BottomNavigationView mBottomNavigationView;
    private LinearLayout couponLinear, orderLinear, helpLinear, pinStoreLinear, aboutLinear, messageLinear, reviewLinear, settingLinear;
    private ViewPager mViewPager;
    private ViewPagerAdaptorHome viewPagerAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_feed);
        __INIT__();
        BottomNavigationViewListener();
        BottomNavHelper.SetupBottomNavigationView(mBottomNavigationView, R.id.nav_account);

        couponLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCouponsFragment();
            }
        });
        orderLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMyOrderactivity();
            }
        });
        messageLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMessageActivity();
            }
        });
        helpLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addHelpActivity();
            }
        });
        aboutLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAboutDeveloperActivity();
            }
        });
        reviewLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMyReviewActivity();
            }
        });
        settingLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSettingActivity();
            }
        });
        pinStoreLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPinStoredActivity();
            }
        });
    }

    private void addMyOrderactivity() {
        Intent intent = new Intent(this, MyOrders.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void addCouponsFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.layout_frame, new CoupponFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    private void addMyReviewActivity() {
        Intent intent = new Intent(this, MyReview.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void addAboutDeveloperActivity() {
        Intent intent = new Intent(this, AboutDeveloper.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void addHelpActivity() {
        Intent intent = new Intent(this, Help.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void addSettingActivity() {
        Intent intent = new Intent(this, Settings.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void addMessageActivity() {
        Intent intent = new Intent(this, MyMessages.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void addPinStoredActivity() {
        Intent intent = new Intent(this, PinStored.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void __INIT__() {
        pinStoreLinear = findViewById(R.id.layout_pin_store);
        reviewLinear = findViewById(R.id.layout_review);
        settingLinear = findViewById(R.id.layout_settings);
        helpLinear = findViewById(R.id.layout_help);
        messageLinear = findViewById(R.id.layout_message);
        aboutLinear = findViewById(R.id.layout_about);
        orderLinear = findViewById(R.id.layout_orders);
        couponLinear = findViewById(R.id.coupon_linear);
        mBottomNavigationView = findViewById(R.id.bottomnav);
    }

    /**
     * A listener for bottom navigation
     * start the activity which is click
     * do not start new activity rather bring it front if exists
     */
    private void BottomNavigationViewListener() {
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(AccountFeed.this, HomeFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_search:
                        startActivity(new Intent(AccountFeed.this, SearchFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_fav:
                        startActivity(new Intent(AccountFeed.this, FavouriteFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_shop:
                        startActivity(new Intent(AccountFeed.this, ShopFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;

                    case R.id.nav_account:
                        return true;

                }
                return false;
            }
        });
    }

}