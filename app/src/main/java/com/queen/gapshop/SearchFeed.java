package com.queen.gapshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.queen.gapshop.Utils.BottomNavHelper;

public class SearchFeed extends AppCompatActivity {

    private BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_feed);
        __INIT__();
        BottomNavHelper.SetupBottomNavigationView(mBottomNavigationView,R.id.nav_search);
        BottomNavigationViewListener();
    }

    private void __INIT__() {
        mBottomNavigationView = findViewById(R.id.bottomnav);
    }

    // baba g neend a rahi hai
    // bahut sekht neend a rahi hai bata nai skta
    // tu asy kun kerty ho
    // kasy kerta hn
    // subha kun nahi uth patay
    // ajj kia meri terf sy neend ayi hai tmhen
    /**
     * A listener for bottom navigation
     * start the activity which is click
     * do not start new activity rather bring it front if exists
     */
    private void BottomNavigationViewListener() {
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(SearchFeed.this, HomeFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));

                        return true;
                    case R.id.nav_search:
                        return true;
                    case R.id.nav_fav:
                        startActivity(new Intent(SearchFeed.this, FavouriteFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_shop:
                        startActivity(new Intent(SearchFeed.this, ShopFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;
                    case R.id.nav_account:
                        startActivity(new Intent(SearchFeed.this, AccountFeed.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        return true;

                }
                return false;
            }
        });
    }

}